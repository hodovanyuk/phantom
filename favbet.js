/**
 * Created by xps on 08.07.2016.
 */
console.log('Loading favbet page');
var webpage = require("webpage");
var fs = require('fs');
var server_url = 'http://localhost:8080/getsourcedata/'

var page = webpage.create();
page.settings.resourceTimeout = 10000;
page.viewportSize = {
  width: 1200,
  height: 800
};
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36';
page.settings.javascriptEnabled = true;
page.settings.loadImages = true;//Script is much faster with this field set to false
phantom.cookiesEnabled = true;
phantom.javascriptEnabled = true;

var url = [];
var i = 0;
url[0] = 'https://www.favbet.com/en/bets/#tour=17351&event=5162924';
// url[1] = 'https://www.favbet.com/en/bets/#tour=17351&event=5162932';
// url[2] = 'https://www.favbet.com/en/bets/#tour=17351&event=5162917';
// url[3] = 'https://www.favbet.com/en/bets/#tour=17351&event=5162918';
// url[4] = 'https://www.favbet.com/en/bets/#tour=17351&event=5162926';
// url[5] = 'https://www.favbet.com/en/bets/#tour=17351&event=5162936';
// url[6] = 'https://www.favbet.com/en/bets/#tour=17351&event=5162940';
// url[7] = 'https://www.favbet.com/en/bets/#tour=17351&event=5162916';
// url[8] = 'https://www.favbet.com/en/bets/#tour=17351&event=5162920';
// url[9] = 'https://www.favbet.com/en/bets/#tour=17351&event=5162922';
// page.captureContent = [/text/, /html/]
page.onResourceReceived = function(res) {
  // if(res['contentType'] === 'application/json' && res['stage'] === 'end'){
  //     console.log('Receive ' + JSON.stringify(res, undefined, 4));
  //     // console.log('image here')
  // }else{
  //     // console.log('NOT image here')

  // }
  // 
};

// page.onResourceRequested = function(requestData, networkRequest) {
//   console.log('Request (#' + requestData.id + '): ' + JSON.stringify(requestData));
// };
 page.onConsoleMessage = function(msg, lineNum, sourceId) {
  console.log('CONSOLE: ' + msg + ' (from line #' + lineNum + ' in "' + sourceId + '")');
};

page.onInitialized = function() {
  page.evaluate(function() {
  console.log('my doc ready')
  document.onreadystatechange = function(){
     (function(window, debug){
    function args(a){
      var s = "";
      for(var i = 0; i < a.length; i++) {
        s += "\t\n[" + i + "] => " + a[i];
      }
      return s;
    }
    var _XMLHttpRequest = window.XMLHttpRequest;

    window.XMLHttpRequest = function() {
      this.xhr = new _XMLHttpRequest();
    }

    // proxy ALL methods/properties
    var methods = [ 
      "open", 
      "abort", 
      "setRequestHeader", 
      "send", 
      "addEventListener", 
      "removeEventListener", 
      "getResponseHeader", 
      "getAllResponseHeaders", 
      "dispatchEvent", 
      "overrideMimeType"
    ];
    methods.forEach(function(method){
      window.XMLHttpRequest.prototype[method] = function() {
        if (debug) console.log("ARGUMENTS", method, args(arguments));
        if (method == "open") {
          this._url = arguments[1];
        }
        return this.xhr[method].apply(this.xhr, arguments);
      }
    });

    // proxy change event handler
    Object.defineProperty(window.XMLHttpRequest.prototype, "onreadystatechange", {
      get: function(){
        // this will probably never called
        return this.xhr.onreadystatechange;
      },
      set: function(onreadystatechange){
        var that = this.xhr;
        var realThis = this;
        that.onreadystatechange = function(){
          // request is fully loaded
          if (that.readyState == 4) {
            if (debug) console.log("RESPONSE RECEIVED:", typeof that.responseText == "string" ? that.responseText.length : "none");
            // there is a response and filter execution based on url
            if (that.responseText && realThis._url.indexOf("whatever") != -1) {
              window.myAwesomeResponse = that.responseText;
            }
          }
          onreadystatechange.call(that);
        };
      }
    });

    var otherscalars = [
      "onabort",
      "onerror",
      "onload",
      "onloadstart",
      "onloadend",
      "onprogress",
      "readyState",
      "responseText",
      "responseType",
      "responseXML",
      "status",
      "statusText",
      "upload",
      "withCredentials",
      "DONE",
      "UNSENT",
      "HEADERS_RECEIVED",
      "LOADING",
      "OPENED"
    ];
    otherscalars.forEach(function(scalar){
      Object.defineProperty(window.XMLHttpRequest.prototype, scalar, {
        get: function(){
          return this.xhr[scalar];
        },
        set: function(obj){
          this.xhr[scalar] = obj;
        }
      });
    });
  })(window, false);
  }

  });
};
pars(url);
function pars(arr) {
  page.open(arr[i], function (status) {
    if (status !== 'success') {
      console.log('Unable to load the address!');
    }
    else  {
      replaceXHR()
      console.log(status + ' = Open login form');
      page.evaluate(function () {
        var login = document.querySelector('a.loginpagecl.but-blue-1 > span');
        var ev = document.createEvent("MouseEvent");
        ev.initMouseEvent(
          "click",
          true /* bubble */, true /* cancelable */,
          window, null,
          0, 0, 0, 0, /* coordinates */
          false, false, false, false, /* modifier keys */
          0 /*left*/, null
        );
        login.dispatchEvent(ev);
      });
      page.render('favbet_data/'+i+'.png');
      fs.write('favbet_data/france'+i+'_page.html', page.content, 'w');
      var regexText = /<li data-marketgroupsid="190" data-mgmo="20" data-clue="Handicap" data-reactid="(.*?)"><div class=".*?" data-reactid=".*?"><span title="Show\/Hide" class=".*?" data-reactid=".*?"><\/span><span rel="mname" data-reactid=".*?">Handicap<\/span><b title=".*?" rel="amtfv" data-mtid=".*?" data-rtid=".*?" data-reactid=".*?"><i class="fa fa-star-o" data-reactid=".*?"><\/i><\/b><\/div><ul class="markets" data-reactid=".*?"><li class="mrkts" data-mid=".*?" data-mo=".*?" data-rtid=".*?" data-reactid=".*?"><ul class="market mrk-itm cl oc2" data-reactid=".*?"><li class="bets_fullmark_body" data-reactid=".*?"><label class="" data-reactid=".*?"><span class="bets_oc ttt" data-reactid=".*?">(.*?)<\/span><button class="betbut a" title=".*?" data-reactid=".*?">(.*?)<\/button><\/label><\/li><li class="bets_fullmark_body" data-reactid=".*?"><label class="" data-reactid=".*?"><span class="bets_oc ttt" data-reactid=".*?">(.*?)<\/span><button class="betbut a" title=".*?" data-reactid=".*?">(.*?)<\/button><\/label><\/li><\/ul><\/li><li class="mrkts" data-mid=".*?" data-mo=".*?" data-rtid=".*?" data-reactid=".*?"><ul class="market mrk-itm cl oc2" data-reactid=".*?"><li class="bets_fullmark_body" data-reactid=".*?"><label class="" data-reactid=".*?"><span class="bets_oc ttt" data-reactid=".*?">(.*?)<\/span><button class="betbut a" title=".*?" data-reactid=".*?">(.*?)<\/button><\/label><\/li><li class="bets_fullmark_body" data-reactid=".*?"><label class="" data-reactid=".*?"><span class="bets_oc ttt" data-reactid=".*?">(.*?)<\/span><button class="betbut a" title=".*?" data-reactid=".*?">(.*?)<\/button><\/label><\/li><\/ul><\/li><li class="mrkts" data-mid=".*?" data-mo=".*?" data-rtid=".*?" data-reactid=".*?"><ul class="market mrk-itm cl oc2" data-reactid=".*?"><li class="bets_fullmark_body" data-reactid=".*?"><label class="" data-reactid=".*?"><span class="bets_oc ttt" data-reactid=".*?">(.*?)<\/span><button class="betbut a" title=".*?" data-reactid=".*?">(.*?)<\/button><\/label><\/li><li class="bets_fullmark_body" data-reactid=".*?"><label class="" data-reactid=".*?"><span class="bets_oc ttt" data-reactid=".*?">(.*?)<\/span><button class="betbut a" title=".*?" data-reactid=".*?">(.*?)<\/button><\/label><\/li><\/ul><\/li><li class="mrkts" data-mid=".*?" data-mo=".*?" data-rtid=".*?" data-reactid=".*?"><ul class="market mrk-itm cl oc2" data-reactid=".*?"><li class="bets_fullmark_body" data-reactid=".*?"><label class="" data-reactid=".*?"><span class="bets_oc ttt" data-reactid=".*?">(.*?)<\/span><button class="betbut a" title=".*?" data-reactid=".*?">(.*?)<\/button><\/label><\/li><li class="bets_fullmark_body" data-reactid=".*?"><label class="" data-reactid=".*?"><span class="bets_oc ttt" data-reactid=".*?">(.*?)<\/span><button class="betbut a" title=".*?" data-reactid=".*?">(.*?)<\/button><\/label><\/li><\/ul><\/li><li class="mrkts" data-mid=".*?" data-mo=".*?" data-rtid=".*?" data-reactid=".*?"><ul class="market mrk-itm cl oc2" data-reactid=".*?"><li class="bets_fullmark_body" data-reactid=".*?"><label class="" data-reactid=".*?"><span class="bets_oc ttt" data-reactid=".*?">(.*?)<\/span><button class="betbut a" title=".*?" data-reactid=".*?">(.*?)<\/button><\/label><\/li><li class="bets_fullmark_body" data-reactid=".*?"><label class="" data-reactid=".*?"><span class="bets_oc ttt" data-reactid=".*?">(.*?)<\/span><button class="betbut a" title=".*?" data-reactid=".*?">(.*?)<\/button><\/label><\/li><\/ul><\/li><\/ul><\/li>/gmi;
      var match;
      var out_data = {};
      while (( match = regexText.exec(page.content) ) != null) {
        var data = {}
        data['Team1'] = match[1].trim()//th1m0p5 match[2].trim();
        data['T1hm0p5'] = match[2].trim()
        data['Team2'] = match[3].trim()//th2m0p5 match[5].trim();
        data['T2hm0p5'] = match[4].trim()
        data['Team3'] = match[5].trim()//th1m0p5 match[2].trim();
        data['T3hm0p5'] = match[6].trim()
        data['Team4'] = match[7].trim()//th1m0p5 match[2].trim();
        data['T4hm0p5'] = match[8].trim()
        data['Team5'] = match[9].trim()//th1m0p5 match[2].trim();
        data['T5hm0p5'] = match[10].trim()
        data['Team6'] = match[11].trim()//th1m0p5 match[2].trim();
        data['T6hm0p5'] = match[12].trim()
        data['Team7'] = match[13].trim()//th1m0p5 match[2].trim();
        data['T7hm0p5'] = match[14].trim()
        data['Team8'] = match[15].trim()//th1m0p5 match[2].trim();
        data['T8hm0p5'] = match[16].trim()
        data['Team9'] = match[17].trim()//th1m0p5 match[2].trim();
        data['T9hm0p5'] = match[18].trim()
        data['Team10'] = match[19].trim()//th1m0p5 match[2].trim();
        data['T10hm0p5'] = match[20].trim()
        data['Team11'] = match[21].trim()
        data['Source'] = "favbet"
        data['League'] = "france1"
        out_data[i] = data;
      }
      fs.write("favbet_data/out_data.json", JSON.stringify(out_data, null, 2), 'w')
    }
  });
};


function replaceXHR(){
  (function(window, debug){
    function args(a){
      var s = "";
      for(var i = 0; i < a.length; i++) {
        s += "\t\n[" + i + "] => " + a[i];
      }
      return s;
    }
    var _XMLHttpRequest = window.XMLHttpRequest;

    window.XMLHttpRequest = function() {
      this.xhr = new _XMLHttpRequest();
    }

    // proxy ALL methods/properties
    var methods = [ 
      "open", 
      "abort", 
      "setRequestHeader", 
      "send", 
      "addEventListener", 
      "removeEventListener", 
      "getResponseHeader", 
      "getAllResponseHeaders", 
      "dispatchEvent", 
      "overrideMimeType"
    ];
    methods.forEach(function(method){
      window.XMLHttpRequest.prototype[method] = function() {
        if (debug) console.log("ARGUMENTS", method, args(arguments));
        if (method == "open") {
          this._url = arguments[1];
        }
        return this.xhr[method].apply(this.xhr, arguments);
      }
    });

    // proxy change event handler
    Object.defineProperty(window.XMLHttpRequest.prototype, "onreadystatechange", {
      get: function(){
        // this will probably never called
        return this.xhr.onreadystatechange;
      },
      set: function(onreadystatechange){
        var that = this.xhr;
        var realThis = this;
        that.onreadystatechange = function(){
          // request is fully loaded
          if (that.readyState == 4) {
            if (debug) console.log("RESPONSE RECEIVED:", typeof that.responseText == "string" ? that.responseText.length : "none");
            // there is a response and filter execution based on url
            if (that.responseText && realThis._url.indexOf("whatever") != -1) {
              window.myAwesomeResponse = that.responseText;
            }
          }
          onreadystatechange.call(that);
        };
      }
    });

    var otherscalars = [
      "onabort",
      "onerror",
      "onload",
      "onloadstart",
      "onloadend",
      "onprogress",
      "readyState",
      "responseText",
      "responseType",
      "responseXML",
      "status",
      "statusText",
      "upload",
      "withCredentials",
      "DONE",
      "UNSENT",
      "HEADERS_RECEIVED",
      "LOADING",
      "OPENED"
    ];
    otherscalars.forEach(function(scalar){
      Object.defineProperty(window.XMLHttpRequest.prototype, scalar, {
        get: function(){
          return this.xhr[scalar];
        },
        set: function(obj){
          this.xhr[scalar] = obj;
        }
      });
    });
  })(window, false);
}
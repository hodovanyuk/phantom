console.log('Loading pinnacle page');
var page = require('webpage').create();
var fs = require('fs');
var server_url = 'http://localhost:8080/getsourcedata/'
page.viewportSize = {
    width: 1200,
    height: 800
};
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36';
page.settings.javascriptEnabled = true;
page.settings.loadImages = true;//Script is much faster with this field set to false
phantom.cookiesEnabled = true;
phantom.javascriptEnabled = true;

function click(el) {
    var ev = document.createEvent("MouseEvent");
    ev.initMouseEvent(
        "click",
        true /* bubble */, true /* cancelable */,
        window, null,
        0, 0, 0, 0, /* coordinates */
        false, false, false, false, /* modifier keys */
        0 /*left*/, null
    );
    el.dispatchEvent(ev);
}

var url = 'http://www.pinnacle.com/en';
page.open(url, function (status) {
    console.log('Page loading')
});

page.onLoadFinished = function (status) {
    if (status !== 'success') {
        console.log('Unable to load the address!');
    } else {
        page.render('pinnacle_data/1.png');
        var login_url = "https://www.pinnacle.com/login/Failed/Classic/en-GB";
        page.open(login_url, function (status) {
            console.log('Page login loading')
        });
        page.onLoadFinished = function (status) {
            if (status !== 'success') {
                console.log('Unable to load login page!');
            } else {
                page.render('pinnacle_data/2.png');
                //http://code-epicenter.com/how-to-login-amazon-using-phantomjs-working-example/
                page.evaluate(function () {
                    var customer = document.querySelector('input.customerId');
                    customer.value = "OO888392"
                    var password = document.querySelector('input.password');
                    password.value = "osathe123!"
                    var login = document.querySelector('input.loginSubmit');
                    login.click()
                    return;
                });
                window.setTimeout(function () {
                    page.render('pinnacle_data/3.png');
                    page.evaluate(function () {
                        var continue_bnt = document.querySelector('a.general_button.green.loginNext');
                        continue_bnt.click()
                    });
                    window.setTimeout(function () {
                        page.render('pinnacle_data/4.png');
                        var prl_url = "https://beta.pinnacle.com/en/Sports/29/Leagues/2036"
                        page.open(prl_url, function (status) {
                            console.log('France1 league loading')
                        });
                        page.onLoadFinished = function (status) {
                            if (status !== 'success') {
                                console.log('Unable to load france1 page!');
                            } else {
                                window.setTimeout(function () {
                                    page.render('pinnacle_data/5.png');
                                    fs.write('pinnacle_data/france1_page.html', page.content, 'w');
                                    var regexText = /LIVE_STATUSES.DEADBALL_WITH_LIVE}" title="(.*)"/gmi;
                                    var match;
                                    var teams = [];
                                    while (( match = regexText.exec(page.content) ) != null) {
                                        teams.push(match[1])
                                    }

                                    var out_data = {};

                                    if (teams !== null) {
                                        var index = 0;
                                        for ( var prop = 0; prop < teams.length; prop = prop + 2) {
                                            var pair = {}
                                            var betregexText = new RegExp("title=\"" + teams[prop] + "\"[^]+?<span class=\"ng-binding\">" +
                                                "([^]+?)<span class=\"better-odds\">", "gmi");
                                            var bet;
                                            bet = betregexText.exec(page.content)
                                            pair['Team1'] = teams[prop]
                                            pair['T1hm0p5'] = bet[1].trim();
                                            betregexText = new RegExp("title=\"" + teams[prop + 1] + "\"[^]+?<span class=\"ng-binding\">" +
                                                "([^]+?)<span class=\"better-odds\">", "gmi");
                                            bet = betregexText.exec(page.content)
                                            pair['Team2'] = teams[prop + 1]
                                            pair['T2hm0p5'] = bet[1].trim();
                                            betregexText = new RegExp("title=\""+pair['Team1']+"\"[^]+?"+pair['Team2']+"[^]+?Draw[^]+?" +
                                                "<span class=\"ng-binding\">([^]+?)<span class=\"better-odds\">", "gmi");
                                            var draw = betregexText.exec(page.content)
                                            pair['Draw'] = draw[1].trim();
                                            pair['Source'] = "pinnacle"
                                            pair['League'] = "france1"
                                            out_data[index] = pair;
                                            index++;

                                        }
                                        fs.write("pinnacle_data/out_data.json",JSON.stringify(out_data,null, 2) , 'w')
                                        var postBody =  JSON.stringify(out_data, null, 2)
                                        page.open(server_url, 'POST', postBody, function (status) {

                                        });
                                        page.onLoadFinished = function (status) {
                                            if (status !== 'success') {
                                                console.log(status + ': Unable to send data!');
                                                phantom.exit()
                                            } else {
                                                console.log('Send data: ' + status);
                                                console.log(page.content)
                                                phantom.exit()
                                            }
                                        }
                                    }

                                }, 5000);
                            }
                        }
                    }, 10000);


                }, 10000);


            }
        }
    }
}
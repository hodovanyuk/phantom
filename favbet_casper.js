/**
 * Created by xps on 17.07.2016.
 */
var casper = require('casper').create();
casper.start('https://www.favbet.com/en/bets/');

casper.then(function() {
    this.capture('favbet_data/1_casper.png', {
        top: 0,
        left: 0,
        width: 1280,
        height: 1280,
    });
    this.echo('First Page: ' + this.getTitle());
});

casper.thenOpen('http://phantomjs.org', function() {
    this.echo('Second Page: ' + this.getTitle());
});

casper.run();